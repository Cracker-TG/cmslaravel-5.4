@if ( $itemTreeView->count() > 1)

  <h1 class="page-header">
      Sort menu
    </h1>


    <div class="tree">
      <ul>
        <li>
            {!! Form::open(['route' => 'order.menu']) !!}
            {!! Form::hidden('csrf-token', csrf_token(), ['id' => 'csrf-token']) !!}
          <a><span><i class="fa fa-book"></i>  Root</span></a>
          <ul class="list-page">

            @foreach($itemTreeView as $i) @if($i->parent_id == 0)
            <li id="list-{{$i->id}}" class="row-{{$i->id}}">
              @if($i->subMenus->count() >= 1)
              <a><span><i class="fa fa-folder"></i>  {{$i->title}} [dropdown]</span></a>
              @else
                <a><span><i class="fa fa-folder"></i>  {{$i->title}} [Link] </span></a>
              @endif
              <ul class="list-page">
                @foreach($itemTreeView as $c) @if($c->parent_id == $i->id && $c->subMenus->count() >= 1)
                <li id="list-{{$c->id}}" class="row-{{$c->id}}">
                  <a><span><i class="fa fa-th"></i>  {{$c->title}} [header]</span></a>
                    <ul class="list-page">
                      @foreach($itemTreeView as $d) @if($d->parent_id == $c->id)
                      <li id="list-{{$d->id}}" class="row-{{$d->id}}">
                          <a><span><i class="fa fa-file"></i>  {{$d->title}}</span></a>
                      </li>
                      @endif @endforeach
                    </ul>
                </li>
                @elseif($c->parent_id == $i->id )
                <li id="list-{{$c->id}}" class="row-{{$c->id}}">
                  <a><span><i class="fa fa-file"></i>  {{$c->title}} </span></a>
                </li>

                @endif @endforeach
            </li>
            </ul>
            @endif @endforeach


          </ul>
          <hr>
            <button type="button" class="btn-sort-order btn btn-success" name="button">SORT</button>
              {!! Form::close() !!}
        </li>
      </ul>
    </div>


    @endif

<script type="text/javascript">


$(function() {
  $('.list-page').sortable({
    update: function(event, ui) {
      var order = $(this).sortable('serialize');
      $(document).on("click", "button", function() {
        $.ajax({
          type: "POST",
          url: "{{ url('admin/menus/order') }}",
          data: {
            list: order,
            _token: "{{ csrf_token() }}"
          },

        });
      });
    }
  });
  $('.btn-sort-order').on('click', function() {
    $.notify({
            // options
            icon: 'fa fa-check-circle',
            title: 'Success',
            message: "Sorted menu was successfully save!",

            target: '_blank'
        },{
            // settings
            element: 'body',
            position: null,
            type: "success",
            allow_dismiss: true,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "right"
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 6000,
            timer: 70,
            url_target: '_blank',
            mouse_over: 'pause',
            animate: {
              enter: 'animated flipInX',
              exit: 'animated flipOutX'
            },
            onShow: null,
            onShown: null,
            onClose: null,
            onClosed: null,
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<p data-notify="message">{2}</p>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
  });
});
</script>
