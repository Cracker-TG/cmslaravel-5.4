
<div class="row">
  <div class="col-md-6">
    <div class="form-group">

      <label>Title</label> {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Link</label> {!! Form::text('path', null, ['class' => 'form-control']) !!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Script</label> {!! Form::textarea('script', null, ['class' => 'form-control']) !!}
    </div>
  </div>

</div>

<div class="row">
  <div class="col-md-12">
      <div class="form-group">
        <label>Main Menu</label> {!! Form::select('parent_id', $list_main_menu, null, ['class' => 'form-control']) !!}
      </div>

</div>
</div>
<div class="row">
    <div class="col-md-12">
  <div class="form-group">
    {!! Form::submit($submit_text, ['class' => 'btn btn-success', 'style' => 'width:100%']) !!}
  </div>
</div>
</div>
