@extends('layouts.backend') @section('content') @include('layouts.partial_page.model_delete')


<div class="row">
<div class="col-md-8">
  <h1 class="page-header">Menus
    <div class="pull-right">
      <a href="{!! url('admin/menus/create') !!}"><button type="button" class="btn btn-success">NEW</button></a>
    </div>

  </h1>

  <table class="table table-striped">
    <thead>
      <tr>
        <th>Title</th>
        <th>Link</th>
        <th>Parent</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($items as $i)
      <tr>
        <td>{{ $i->title }}</td>
        <td>{{ $i->path}}</td>
        <td>{{ $i->parent['title'] }}</td>

        <td>
          <a href="{{ route('menus.edit', $i->id) }}"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button></a>
        </td>
        </td>
        <td>
          {{ Form::open(['method' => 'DELETE', 'route' => ['menus.destroy', $i->id], 'class' => 'form-delete']) }} {{ Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger btn-xs delete ', 'name' => 'delete_modal']) }} {{ Form::close()
          }}

        </td>
      </tr>

      @endforeach

    </tbody>
  </table>
  {{$items->render() }}
</div>
<div class="col-md-4">

  @include('backend.menus._sort_menu')
</div>
</div>


@endsection
