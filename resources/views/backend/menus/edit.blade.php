@extends('layouts.backend')
@section('content')
<h1 class="page-header">Create User</h1>

{!! Form::model($menu, ['route' => ['menus.update', $menu->id], 'method' => 'PATCH']) !!}

			@include('backend.menus.form', ['submit_text' => ('Save')])

{!! Form::close() !!}

@endsection
