@extends('layouts.backend')
@section('content')
<h1 class="page-header">Create Menu

</h1>




{!! Form::model($menu = new App\Models\Menu, ['route' => ['menus.store']]) !!}


  @include('backend.menus.form', ['submit_text' => ('Save')])

{!! Form::close() !!}

@endsection
