@extends('layouts.backend')
@section('content')
<h1 class="page-header">Edit Page</h1>

{!! Form::model($page, ['route' => ['pages.update',$page->id], 'method' => 'PATCH']) !!}
  @include('backend.pages.form', ['submit_text' => ('Save')])
{!! Form::close() !!}

@endsection
