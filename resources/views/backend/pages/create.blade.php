@extends('layouts.backend')
@section('content')
@include('layouts.partial_page.model_delete')
<h1 class="page-header">Create User</h1>


{!! Form::model($page = new App\Models\Page, ['route' => ['pages.store'], 'files' => true ,'class'=> 'my-form']) !!}
  @include('backend.pages.form', ['submit_text' => ('Save')])
{!! Form::close() !!}

@endsection
