<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Title</label> {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Short Url</label> {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>Tags</label> {!! Form::text('tags', null, ['class' => 'form-control']) !!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Body</label> {!! Form::textarea('content', null, ['class' => 'form-control body-page']) !!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Script</label>  {!! Form::textarea('script', null, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-6">
      <div class="form-group">
        <label>Style</label> {!! Form::textarea('style', null, ['class' => 'form-control']) !!}
      </div>
  </div>
</div>

<div class="row">
    <div class="col-md-12">
  <div class="form-group">
    {!! Form::submit($submit_text, ['class' => 'btn btn-success', 'style' => 'width:100%']) !!}
  </div>
</div>
</div>


  <script>
  var editor_config = {
  path_absolute : "/",
  selector: ".body-page",
  remove_script_host : 'true',
  plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen",
    "insertdatetime media nonbreaking save table contextmenu directionality",
    "emoticons template paste textcolor colorpicker textpattern jbimages"
  ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages",
    relative_urls: false,
       file_browser_callback: function(field_name, url, type, win) {

       }



};

tinymce.init(editor_config);
  </script>
