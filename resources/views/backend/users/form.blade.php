<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Name</label> {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Lastname</label> {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Email</label> {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Password</label> {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
      <div class="form-group">
        <label>Role</label> {!! Form::select('role_id', $roles, null, ['class' => 'form-control']) !!}
      </div>
  </div>
</div>
<div class="row">
    <div class="col-md-12">
  <div class="form-group">
    {!! Form::submit($submit_text, ['class' => 'btn btn-success', 'style' => 'width:100%']) !!}
  </div>
</div>
</div>
