@extends('layouts.backend')
@section('content')
<h1 class="page-header">Create User</h1>

{!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PATCH']) !!}

			@include('backend.users.form', ['submit_text' => ('Save')])

{!! Form::close() !!}

@endsection
