{!! Form::open(['route'=>'avatar-upload', 'class' => 'dropzone', 'files'=>true, 'id'=>'my-dropzone']) !!}


{!! Form::close() !!}

{!! Form::hidden('csrf-token', csrf_token(), ['id' => 'csrf-token']) !!}

<script>

  Dropzone.options.myDropzone = {
    addRemoveLinks: true,
    acceptedFiles: 'image/*',
    url: "/admin/users/upload",
    dictRemoveFile: 'Remove',
    maxFiles: 1,
    init: function(){
      this.on("maxfilesexceeded", function(file){
            this.removeAllFiles();
            this.addFile(file);
      });
      this.on("removedfile", function(file) {

        $.ajax({
                type: 'POST',
                url: '{{ url('admin/users/upload/delete') }}',
                data: {id: file.name, _token: "{{ csrf_token() }}"},
                dataType: 'html',

            });

        } );
}

  };
</script>
