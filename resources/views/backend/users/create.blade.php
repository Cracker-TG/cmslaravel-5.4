@extends('layouts.backend')
@section('content')
<h1 class="page-header">Create User

</h1>




{!! Form::model($user = new App\Models\User, ['route' => ['users.store'], 'files' => true]) !!}

  @include('backend.users.form', ['submit_text' => ('Save')])
{!! Form::close() !!}

@endsection
