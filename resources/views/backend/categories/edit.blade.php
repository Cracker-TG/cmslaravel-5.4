@extends('layouts.backend')
@section('content')
<h1 class="page-header">Create User</h1>

{!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'PATCH']) !!}

			@include('backend.categories.form', ['submit_text' => ('Save')])

{!! Form::close() !!}

@endsection
