@extends('layouts.backend') @section('content') @include('layouts.partial_page.model_delete')


<div class="row">
<div class="col-md-12">
  <h1 class="page-header">Categories
    <div class="pull-right">
      <a href="{!! url('admin/categories/create') !!}"><button type="button" class="btn btn-success">NEW</button></a>
    </div>

  </h1>
  <table class="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th></th>
           <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($items as $i)
        <tr>
          <td>{{ $i->name }}</td>
          <td>
              <a href="{{ route('categories.edit', $i->id) }}"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button></a>
          </td>
          <td>
            {{ Form::open(['method' => 'DELETE', 'route' => ['categories.destroy', $i->id], 'class' => 'form-delete']) }}
              {{ Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger btn-xs delete ', 'name' => 'delete_modal']) }}
            {{ Form::close() }}
          </td>
        </tr>

        @endforeach

      </tbody>
    </table>


@endsection
