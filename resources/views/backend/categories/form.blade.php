
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Name</label> {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
  </div>
</div>
<div class="row">
    <div class="col-md-12">
  <div class="form-group">
    {!! Form::submit($submit_text, ['class' => 'btn btn-success', 'style' => 'width:100%']) !!}
  </div>
</div>
</div>
