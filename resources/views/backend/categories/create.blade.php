@extends('layouts.backend')
@section('content')
<h1 class="page-header">Create Category</h1>




{!! Form::model($cateory = new App\Models\Category, ['route' => ['categories.store']]) !!}


  @include('backend.categories.form', ['submit_text' => ('Save')])

{!! Form::close() !!}

@endsection
