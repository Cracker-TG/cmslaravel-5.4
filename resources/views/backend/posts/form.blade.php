<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Title</label> {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Url Link</label> {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>tags</label> {!! Form::text('tags', null, ['class' => 'form-control']) !!}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-3 ">
    <div class="form-group">
      <label>Cover</label> {!! Form::file('cover', ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Publish</label> {!! Form::hidden('is_visible',0) !!} {!! Form::checkbox('is_visible',1, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-6">
  <div class="form-group">
      <label>Category</label>
      @if(!isset($edit_categories))
        @foreach($categories as $c)
        <div class="checkbox">
          <label>   {{ Form::checkbox('category_id[]', $c->id,'') }}  {{ $c->name }}</label>
        </div>
      @endforeach
      @else
      @foreach($categories as $c)
      <div class="checkbox">
        <label>   {{ Form::checkbox('category_id[]', $c->id,in_array($c->id, $edit_categories)) }}  {{ $c->name }}</label>
      </div>
    @endforeach
    @endif
</div>

</div>
<div class="row">


</div>
<div class="row">
    <div class="col-md-12">
      <label>Summary</label> {!! Form::textarea('summary', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
      <label>Content</label> {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
  <div class="form-group">
    {!! Form::submit($submit_text, ['class' => 'btn btn-success', 'style' => 'width:100%']) !!}
  </div>
</div>
</div>
