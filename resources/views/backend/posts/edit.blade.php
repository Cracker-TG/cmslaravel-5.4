@extends('layouts.backend')
@section('content')
<h1 class="page-header">Edit Post</h1>

{!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PATCH','files'=>true]) !!}

			@include('backend.posts.form', ['submit_text' => ('Save')])

{!! Form::close() !!}

@endsection
