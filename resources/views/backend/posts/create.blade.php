@extends('layouts.backend')
@section('content')
<h1 class="page-header">Create Post</h1>
{!! Form::model($post = new App\Models\Post, ['route' => ['posts.store'],'files' => true]) !!}
  @include('backend.posts.form', ['submit_text' => ('Save')])
{!! Form::close() !!}
@endsection
