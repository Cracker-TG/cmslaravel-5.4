@extends('layouts.backend') @section('content') @include('layouts.partial_page.model_delete')


<div class="row">
<div class="col-md-12">
  <h1 class="page-header">Posts
    <div class="pull-right">
      <a href="{!! url('admin/posts/create') !!}"><button type="button" class="btn btn-success">NEW</button></a>
    </div>

  </h1>

  <table class="table table-striped">
      <thead>
        <tr>
          <th>Id</th>
          <th>Title</th>
          <th>Publish</th>
          <th></th>
           <th></th>

        </tr>
      </thead>
      <tbody>
        @foreach($items as $i)
        <tr>
          <td>{{ $i->id }}</td>
          <td>{{ $i->title }}</td>
          @if($i->is_visible == 1)
          <td>publish</td>
          @else
          <td>non publish</td>
          @endif
          <td>
              <a href="{{ route('posts.edit', $i->id) }}"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button></a>
          </td>
          <td>

            {{ Form::open(['method' => 'DELETE', 'route' => ['posts.destroy', $i->id], 'class' => 'form-delete']) }}
              {{ Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger btn-xs delete ', 'name' => 'delete_modal']) }}
            {{ Form::close() }}

          </td>
        </tr>

        @endforeach

      </tbody>
    </table>


@endsection
