@extends('layouts.backend')
@section('content')
<h1 class="page-header">Edit Page</h1>

{!! Form::model($items, ['route' => ['slides.update',$items->id], 'method' => 'PATCH']) !!}
  @include('backend.slides.form', ['submit_text' => ('Save')])
{!! Form::close() !!}

<script type="text/javascript">
$("div#dropzone").dropzone({
}
});


</script>
@endsection
