@extends('layouts.backend')

@section('content')

@include('layouts.partial_page.model_delete')

<h1 class="page-header">Slides Image
  <div class="pull-right">
    <a href="{!! url('admin/slides/create') !!}"><button type="button" class="btn btn-success">NEW</button></a>
  </div>
</h1>
<table class="table table-striped">
    <thead>
      <tr>
        <th>Title</th>
        <th></th>
         <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($items as $i)
      <tr>
        <td>{{ $i->title }}</td>
        <td>
            <a href="{{ route('slides.edit', $i->id) }}"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button></a>
        </td>
        <td>
          {{ Form::open(['method' => 'DELETE', 'route' => ['slides.destroy', $i->id], 'class' => 'form-delete']) }}
            {{ Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger btn-xs delete ', 'name' => 'delete_modal']) }}
          {{ Form::close() }}
        </td>
      </tr>

      @endforeach

    </tbody>
  </table>

@endsection
