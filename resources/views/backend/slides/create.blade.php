@extends('layouts.backend')
@section('content')
<h1 class="page-header">Create User

</h1>




{!! Form::model($slide = new App\Models\Slide, ['route' => ['slides.store' ,'id' => 'form-dropzone'], 'files' => true ]) !!}
@include('backend.slides.form', ['submit_text' => ('Save')])
{!! Form::close() !!}



<script>

$("div#dropzone").dropzone({
  url: "{{route('slides.store')}}",
  headers:{  'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
  autoProcessQueue: false,
  uploadMultiple: true,
  parallelUploads: 100,
  maxFiles: 100,
  addRemoveLinks: true,

  init: function() {

    var $this = this;
    $("input[type=submit").click(function(e) {
      e.preventDefault();
      e.stopPropagation();


      if($this.files.length > 0)
      {

        $this.processQueue();
      }
      else
      {
        $( "#form-dropzone" ).submit();
      }

    });
    this.on("sendingmultiple", function(file, xhr, formData) {

       formData.append("title", $('#title').val());
       formData.append("content", $('#content').val());
       formData.append("script", $('#script').val());
       formData.append("style", $('#style').val());
       formData.append("page_id", $("#menu").val());
       formData.append("is_visible", $("#is_visible").is(':checked') ? 1 : 0);

    });

    this.on("successmultiple", function(files, response) {

      location = '{{{ action('Backend\SlidesController@index') }}}';
    });
  },

});
Dropzone.autoDiscover = false;

</script>
@endsection
