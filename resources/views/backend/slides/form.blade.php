<div class="row">
  <div class="col-md-6">
      <div class="form-group">
        <label>Title</label> {!! Form::text('title', null, ['class' => 'form-control','id'=>'title']) !!}
      </div>
</div>
  <div class="col-md-4">
      <div class="form-group">
        <label>Menu</label> {!! Form::select('page_id', $list_page, null, ['class' => 'form-control','id'=>'menu']) !!}
      </div>
</div>
<div class="col-md-2">
  <div class="form-group">
    <label>Visible</label>
    <input checked="checked" name="is_visible" type="checkbox" value="1" id="is_visible">
  </div>
</div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="dropzone" id="dropzone" name="mainFileUploader" >
    </div>
  </div>
</row>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Content</label> {!! Form::textarea('content', null, ['class' => 'form-control body-page','id'=>'content']) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Script</label>  {!! Form::textarea('script', null, ['class' => 'form-control','id'=>'script']) !!}
    </div>
  </div>
  <div class="col-md-6">
      <div class="form-group">
        <label>Style</label> {!! Form::textarea('style', null, ['class' => 'form-control','id'=>'style']) !!}
      </div>
  </div>
</div>
<div class="row">
    <div class="col-md-12">
  <div class="form-group">
    {!! Form::submit($submit_text, ['class' => 'btn btn-success submit-form', 'style' => 'width:100%']) !!}
  </div>
</div>
</div>

<script type="text/javascript">



</script>
