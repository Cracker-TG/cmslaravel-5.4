@extends('layouts.backend')

@section('content')
<h1 class="page-header">Setting Header Footer

</h1>

{!! Form::model($get,['route' => ['HFU'], 'files' => true ,'class'=> 'my-form','method' => 'patch']) !!}
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Header</label> {!! Form::textarea('header', null, ['class' => 'form-control text-header-footer']) !!}
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Footer</label> {!! Form::textarea('footer', null, ['class' => 'form-control text-header-footer']) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Script</label>  {!! Form::textarea('script', null, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-6">
      <div class="form-group">
        <label>Style</label> {!! Form::textarea('style', null, ['class' => 'form-control']) !!}
      </div>
  </div>
</div>

<div class="row">
    <div class="col-md-12">
  <div class="form-group">
    {!! Form::submit("save", ['class' => 'btn btn-success', 'style' => 'width:100%']) !!}
  </div>
</div>
</div>

{!! Form::close() !!}

@endsection
