@extends('layouts.backend')

@section('content')
<h1 class="page-header">Setting Header Footer

</h1>

{!! Form::model($get,['route' => ['logo-update'], 'files' => true ,'class'=> 'my-form','method' => 'patch']) !!}
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label>Logo</label> {!! Form::file('image', null, ['class' => 'form-control text-header-footer']) !!}
    </div>
  </div>
</div>


<div class="row">
    <div class="col-md-12">
  <div class="form-group">
    {!! Form::submit("save", ['class' => 'btn btn-success', 'style' => 'width:100%']) !!}
  </div>
</div>
</div>

{!! Form::close() !!}

@endsection
