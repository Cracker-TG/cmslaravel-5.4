@extends('layouts.auth')

@section('content')
<div class="panel panel-default">
          <div class="panel-heading">
          <h3 class="panel-title">Please sign in</h3>
      </div>
        <div class="panel-body">
          <form class="" method="POST" action="{{ route('login') }}">

              {{ csrf_field() }}

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">



                      <input id="email" type="email" class="form-control"  placeholder="E-mail" name="email" value="{{ old('email') }}" required autofocus>

                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif

              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">


                      <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>

                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif

              </div>


                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                          </label>
                      </div>


                      <button type="submit" class="btn btn-lg btn-success btn-block">
                          Login
                      </button>


                  </div>
              </div>
      
          </form>
        </div>
    </div>


@endsection
