
          <div class="owl-carousel owl-theme" id="owl-demo">
            @foreach($p->slide as $slide)
            @if($slide->is_visible == true)
              @foreach(explode('|', $slide->image) as $info)
                <div class="item">
                  <img src="uploads/slideImages/{{ $info }}" />
                </div>

              @endforeach
              @endif
            @endforeach
            </div>


  <script type="text/javascript">
  $(document).ready(function() {
    $('#owl-demo').owlCarousel({
      loop: true,
      autoplay:true,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
autoplayTimeout:5000,
 autoplaySpeed: 1000,
      margin: 20,
      singleItem: true,
      items: 1
    });
  });
  </script>
