<!-- Navigation -->

<nav class="navbar navbar-default navbar-fixed-top"  id="nav">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <!-- Brand and toggle get grouped for better mobile display -->
  			<div class="navbar-header">
  				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
  					<span class="sr-only">Toggle navigation</span>
  					<span class="icon-bar"></span>
  					<span class="icon-bar"></span>
  					<span class="icon-bar"></span>
  				</button>
          <?php $logo = App\Models\Logo::find(1); ?>
  				<a class="navbar-brand" href="/home"><img src="{{ url('uploads/logo/').'/'.$logo->image }}"> </a>
  			</div>
    <?php $menus = App\Models\Menu::where('parent_id', '0')->orderBy('order', 'asc')->get(); ?>
    <?php $pages = App\Models\Page::get(); ?>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">

        @foreach($menus as $menu) @if($menu->parent_id == 0 && $menu->subMenus->count() == 0 )

          @if(strcasecmp ($menu->title,'Home') == 0)

          @elseif(strcasecmp ($menu->title,'Index') == 0)

          @else
        <li><a class="x" href="{{$menu->path}}">{{ $menu->title}}</a></li>
          @endif
        @else
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $menu->title }}<span class="caret"></span></a>

          <ul class="dropdown-menu">
            <li>
              @foreach($menu->subMenus->sortBy('order') as $submenu) @if($submenu->subMenus->count() >= 1)

              <li class="dropdown-header">{{$submenu->title}}</li>

              <li>
                @foreach($submenu->subMenus->sortBy('order') as $subm)
                <a href="{{$subm->path}}">{{$subm->title}}</a>
                @endforeach
              </li>
              <li class="divider"></li>
              @else
              <li>
                  <a href="{{$submenu->path}}">{{$submenu->title}}</a>
              </li>
              @endif
              @endforeach
            </li>
          </ul>

        </li>
        @endif @endforeach
      @if (Auth::guest())
      @elseif(Auth::user()->role_id == 1)
      <li><a href="{{ url('admin')}}">DASHBOARD</a></li>
      @endif
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>
