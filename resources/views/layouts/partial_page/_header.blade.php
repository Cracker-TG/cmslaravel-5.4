<?php $header = App\Models\HeaderFooter::find(1) ?>
<header class="masthead">
  {!! $header->header !!}
</header>
<script type="text/javascript">
  {!! $header->script !!}
</script>
<style>
  {!! $header->style !!}
</style>
