<?php $footer = App\Models\HeaderFooter::find(1) ?>

  {!! $footer->footer!!}
  <script type="text/javascript">
    {!! $footer->script !!}
  </script>
  <style>
    {!! $footer->style !!}
  </style>
