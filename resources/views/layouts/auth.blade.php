<!DOCTYPE html>
<html>
<head>
  <title> LOGIN CMS </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
  <script src="js/app.js"></script>
<link href="/css/app.css" rel="stylesheet">
</head>
<body>
  <div class="row vertical-offset-100">
    <div class="col-md-4 col-md-offset-4">
  @yield('content')
</div>
</div>

</body>
