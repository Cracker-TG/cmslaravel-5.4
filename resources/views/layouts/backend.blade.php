<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title> BACKEND CMS </title>

      <link rel="stylesheet" href="{{ asset('css/app.css') }}">
      <link rel="stylesheet" href="{{ asset('css/backend.css') }}">
      <script src="{{ asset("js/app.js") }}"></script>
      <script src="{{ asset("js/backend.js") }}"></script>
      <script src="{{ asset("js/tinymce/tinymce.min.js") }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.js"></script>

</head>

<body>
  <div class="wrapper">

    <nav class="navbar navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
        @include('layouts/partial_admin_page/navbar')

        @include('layouts/partial_admin_page/sidebar')
  </nav>
  <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
              <br>
              @include('layouts.partial_page._messages')
    @yield('content')
    </div>
  </div>
</div>






</body>
