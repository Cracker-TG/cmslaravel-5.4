<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a> </li>
      <li> <a href="{{ url('admin/menus') }}"><i class="fa fa-list fa-fw"></i> Menus</a> </li>
      <li > <a href="#"><i class="fa fa-clone fa-fw"></i>  Post<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
          <li> <a href="{{ url('admin/posts/create') }}">New Post</a> </li>
          <li> <a href="{{ url('admin/posts') }}">All Posts</a> </li>
          <li> <a href="{{ url('admin/categories') }}">Categories</a> </li>
        </ul>
      </li>
      <li> <a href="{{ url('admin/pages') }}"><i class="fa fa-file fa-fw"></i> Pages</a> </li>
      <li> <a href="{{ url('admin/slides') }}"><i class="fa fa-sliders fa-fw"></i> Slides</a> </li>
      <li> <a href="#"><i class="fa fa-wrench fa-fw"></i>Setting<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
          <li> <a href="{{ url('admin/setting/headerfooter') }}">Header And Footer</a> </li>
          <li> <a href="{{ url('admin/setting/logo') }}">Logo</a> </li>
        </ul>
      </li>
      <li> <a href="{{ url('admin/users') }}"><i class="fa fa-users fa-fw"></i> Users</a> </li>
    </ul>
  </div>
</div>
