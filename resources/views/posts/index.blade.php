
@extends('layouts.app')
@section('content')

<div class="container">
@if($posts)
@foreach($posts as $post)
  <div class="row">
    <div class="col-md-7">
      <a href="#">
        <img class="img-responsive" src="uploads/postImages/{{ $post->cover }}" style="width:700px;height:300px" alt="" >
      </a>
    </div>
    <div class="col-md-5">
      <h3> {!!$post->title!!}</h3>
      <h4>Posted By : {{$post->user->name}} </h4>
        <p><span class="glyphicon glyphicon-time"></span> Posted on {{$post->published_at}}</p>
      <p>{!! $post->summary !!}</p>
      <a class="btn btn-primary" href="{{ url('posts',[$post->id, $post->slug]) }}">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
  </div>
  <hr>
@endforeach
@endif
</div>

@endsection
