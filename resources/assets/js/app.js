/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */



require('./bootstrap');
require('bootstrap-notify/bootstrap-notify.min.js');
require('jquery-ui/ui/widgets/sortable.js');
//require('dropzone/dist/dropzone.js');


//model popup
$(document).on('click', '.form-delete', function(e) {
  e.preventDefault();
  var $form = $(this);
  $('#confirm').modal({
      backdrop: 'static',
      keyboard: false
    })
    .on('click', '#delete-btn', function() {
      $form.submit();
    });
});


//jquery-ui
$(function() {
  $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
  $('.tree li.parent_li > a').on('click', function(e) {
    var children = $(this).parent('li.parent_li').find(' > ul > li');
    if (children.is(":visible")) {
      children.hide('fast');
      $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
    } else {
      children.show('fast');
      $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
    }
    e.stopPropagation();
  });
});
//function

//nav
