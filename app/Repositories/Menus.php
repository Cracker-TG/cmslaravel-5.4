<?php

namespace App\Repositories;


use App\Models\Menu;

class Menus
{

	public function all()
	{
		return Menu::orderBy('created_at','desc')->paginate(10);
	}

	public function allTreeView()
	{
		return Menu::orderBy('order','asc')->get();
	}
	public function listMenu()
	{
  $list =  Menu::pluck('title', 'id');
  $list_menu = array('0' => 'No main menu') + $list->toArray();

  return $list_menu;
	}

  public function findMenu($id)
  {
    $menu = Menu::find($id);
    return $menu;
  }


}
