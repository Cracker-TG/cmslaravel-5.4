<?php

namespace App\Repositories;


use App\Models\Category;

class Categories
{

	public function all()
	{
		return Category::orderBy('created_at','desc')->paginate(10);
	}

  public function findCategory($id)
  {
    return Category::find($id);
  }


}
