<?php

namespace App\Repositories;


use App\Models\{Post, Category};

class Posts
{

	public function all()
	{
		return Post::get();
	}

	public function findPost($id)
	{
		return Post::find($id);
	}
	public function getCategories()
	{
		return Category::orderBy('created_at','desc')->get();
	}

public function getCategoriesFormRelation($id)
{
			$post = Post::find($id);
	    $categories =  $post->categories()->select('posts_categories.category_id')->pluck('category_id')->toArray();
			return $categories;

}
public function showLastSix()
{
	return Post::latest('published_at')->published()->paginate(6);
}

}
