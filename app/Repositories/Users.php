<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Role;

class Users
{
  /**
  	* Get all
    *
 	*/
	public function all()
	{
		return User::get();
	}


	/**
   * Get name and id as array
   *
   */
	public function listRoles()
	{
	return Role::pluck('name', 'id');

	}

  public function findUser($id)
  {
    $user = User::find($id);
    return $user;
  }

  public function upload()
   {

   }

}
