<?php

namespace App\Repositories;


use App\Models\{Slide, Page};

class Slides
{

	public function all()
	{
		return Slide::orderBy('created_at','desc')->paginate(10);
	}


  public function findSlide($id)
  {
    return Slide::find($id);
  }

	public function PageList()
	{
			$page = Page::pluck('title','id');
			$list = array('0' => 'No main menu') + $page->toArray();
  		return $list;
	}

}
