<?php

namespace App\Repositories;

use App\Models\{HeaderFooter, Logo};


class Setting
{

public function getHeaderFooter($id)
{
  return HeaderFooter::find($id);

}

public function getLogo($id)
{
  return Logo::find($id);

}


}
