<?php

namespace App\Repositories;


use App\Models\{Page, HeaderFooter};

class Pages
{

	public function all()
	{
		return Page::get();
	}


  public function findSubPage($id)
  {
    return Page::where('slug', 'LIKE', '%'.$id.'%')->get();
  }

	public function findPage($id)
	{
		$pages = Page::find($id);
		return $pages;
	}




}
