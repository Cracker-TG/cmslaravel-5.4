<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Page extends Model
{


    protected $table = 'pages';
    protected $fillable = ['title','content','tages','style','script','slug','user_id'];

    public function slide()
    {

      	return $this->hasMany('App\Models\Slide');
    }
}
