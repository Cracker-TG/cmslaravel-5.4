<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HeaderFooter extends Model
{

    protected $table = 'headers-footers';
    protected $fillable = ['header','footer','script','style','user_id'];


}
