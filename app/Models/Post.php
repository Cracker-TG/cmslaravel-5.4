<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Post extends Model
{

    protected $table = 'posts';
    protected $fillable = ['title','summary','content','cover','tages','is_visible','slug','user_id'];

    public function categories()
    {
      return $this->belongsToMany('App\Models\Category', 'posts_categories', 'post_id', 'category_id');
    }
    /* setPublishedAttribute
    *  use this function for showing published_at in Carbon method like created_at and updated_at
    * set the format of the date
    */
    public function setPublishedAtAttribute($date)
    {
     $this->attributes['published_at'] = Carbon::createFromFormat('d.m.Y',$date);
   }
   /*
    * get the published_at attribute
    */
   public function getPublishedAtAttribute($date)
    {
     return Carbon::parse($date)->formatLocalized('%A %d %B %Y');     
    }

    /*
    * published() function in order to show published posts till now
    */
    public function scopePublished($query)
    {
        $query->where('published_at','<=',Carbon::now());
    }
    /*
    *  get the user associated with the given post
    */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
