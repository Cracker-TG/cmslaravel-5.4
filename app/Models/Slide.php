<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slide extends Model
{
	protected $table = 'slides';
	protected $fillable = ['title','image','is_visible','page_id','style','script','content'];
	protected $attributes = array(
	'page_id' => '0',
	'is_visible' => 'boolean',

);

public function page()
{
    return $this->belongsTo('App\Models\Page');
}


}
