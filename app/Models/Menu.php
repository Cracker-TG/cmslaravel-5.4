<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    protected $table = 'menus';
    protected $fillable = ['title','path','script','parent_id','user_id'];
    protected $attributes = array(
		'parent_id' => '0'
	);

    public function parent()
    {
  		return $this->belongsTo('App\Models\Menu', 'parent_id');
  	}
  	public function subMenus()
    {
  		return $this->hasMany('App\Models\Menu', 'parent_id');
  	}


}
