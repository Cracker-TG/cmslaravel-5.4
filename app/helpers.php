<?php
/* Set active class
-------------------------------------------------------- */

    function classActivePath($route1)
    {

        return (\Request::route()->getName() == $route1 ) ? 'active' : '';
    }
