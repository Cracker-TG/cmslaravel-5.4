<?php

namespace App\Http\Controllers;
use \App\Models\Page;
use Illuminate\Http\Request;
use App\Repositories\Pages;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(Pages $pages)
    {
          $this->pages = $pages;

    }

    public function show($id)
    {

          $page = $this->pages->findSubPage($id);
		      return view('pages.show', compact('page'));
    }

    public function getHome()
    {
      $pages = $this->pages->all();
      foreach($pages as $page){

            if($page->slug == 'home')
            {
              $page = $this->pages->findSubPage($page->slug);
              return view('pages.show',compact('page'));
            }
      }
    }
    public function getIndex()
    {

      $pages = $this->pages->all();

      foreach($pages as $page){

            if($page->slug == 'index')
            {
              $page = $this->pages->findSubPage($page->slug);
              return view('index',compact('page'));
            }
      }

    }
}
