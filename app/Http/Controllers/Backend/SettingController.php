<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Setting;

use App\Http\Requests\{HeaderFooterRequest, LogoRequest};
use Hash;
use Auth;
use Session;
use Image;
use File;

class SettingController extends MainController
{

    private $view = 'backend.setting';


    public function __construct(Setting $setting)
    {

        $this->setting = $setting;

    }

    public function headerFooterEdit()
    {
      $get = $this->setting->getHeaderFooter(1);
      return view($this->view.'.'.__FUNCTION__, compact('get'));
    }

    public function headerFooterUpdate(HeaderFooterRequest $request)
    {

      $hf = $this->setting->getHeaderFooter(1);
      $hf->header = $request->header;
      $hf->footer = $request->footer;
      $hf->script = $request->script;
      $hf->style = $request->style;
      $hf->user_id = Auth::user()->id;
      $hf->save();

    Session::flash('success', 'Update Header And Footer was successfully save!');
    return redirect()->back();
  }
  public function logoEdit()
  {
      $get = $this->setting->getLogo(1);
      return view($this->view.'.'.__FUNCTION__,compact('get'));
  }
  public function logoUpdate(LogoRequest $request)
  {
      $logo = $this->setting->getLogo(1);
      if($request->hasFile('image'))
      {
        $path = 'uploads/logo/';
        File::delete($path.$logo->image);
        $image = $request->file('image');
        $image_name = $image->getClientOriginalName();
        $location = public_path($path.$image_name);
        Image::make($image)->save($location);
        $logo->image = $image_name;
      }
      $logo->save();
      Session::flash('success', 'Update Logo was successfully save!');
      return redirect()->back();
  }

}
