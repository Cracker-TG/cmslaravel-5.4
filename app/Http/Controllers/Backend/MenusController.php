<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Menus;
use App\Models\Menu;
use App\Http\Requests\MenuRequest;
use Hash;
use Auth;
use Session;
use Image;
use File;

class MenusController extends MainController
{

    private $view = 'backend.menus';


    public function __construct(Menus $menus)
    {
          $this->menus = $menus;

    }

    public function index()
    {
        $itemTreeView = $this->menus->allTreeView();
        $items = $this->menus->all();
        return view($this->view.'.'.__FUNCTION__, compact('items','itemTreeView'));

    }

    public function create()
    {

      $list_main_menu = $this->menus->listMenu();



      return view($this->view.'.'.__FUNCTION__, compact('list_main_menu'));


    }

    public function store(MenuRequest $request)
    {


      //dd($request);
      $menu = Menu::create($request->except('user_id'));
      $menu->user_id = Auth::user()->id;

      $menu->save();
      Session::flash('success', 'Create menus was successfully save!');
      return redirect()->route('menus.index');

    }

    public function edit($id)
    {
      $list_main_menu = $this->menus->listMenu();

      $menu = $this->menus->findMenu($id);

      return view($this->view.'.'.__FUNCTION__, compact('menu', 'list_main_menu'));
    }

    public function update(MenuRequest $request, $id)
    {

        $menu = $this->menus->findMenu($id);
        $menu->title = $request->title;
        $menu->path = $request->path;
        $menu->script = $request->script;
        $menu->parent_id = $request->parent_id;
        $menu->save();
        Session::flash('success', 'Update menus was successfully save!');
        return redirect()->route('menus.index');

    }

    public function destroy($id)
    {
      $menu = $this->menus->findMenu($id);

      $menu->delete();
      Session::flash('success', 'Destroy user was successfully save!');
      return back();
    }

    public function order(Request $request)
    {
      if($request->ajax()){

        $list = $request->list;

        $output = array();
        $list = parse_str($list, $output);

        $i = 0;

        foreach ($output['list'] as $key => $value)
        {

          Menu::where('id', $value)->update(['order' => $i]);
          $i++;
        }


      }
    }
  }
