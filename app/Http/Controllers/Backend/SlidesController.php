<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Slides;
use App\Models\Slide;
use App\Http\Requests\SlideRequest;
use Hash;
use Auth;
use Session;
use Image;
use File;
use Illuminate\Support\Facades\Input;
use Validator;

class SlidesController extends MainController
{
    private $view = 'backend.slides';


    public function __construct(Slides $slides)
    {
        $this->slide = $slides;
    }


    public function index()
    {
        $items = $this->slide->all();
        return view($this->view.'.'.__FUNCTION__,compact('items'));
    }

    public function create()
    {
        $list_page = $this->slide->PageList();
        return view($this->view.'.'.__FUNCTION__, compact('list_page'));
    }

    public function store(SlideRequest $request)
    {

        $slide = Slide::create($request->except(['file']));
        $images = $request->file('file');
        $img_array=array();
        foreach ($images as $image) {
            $image_name = $image->getClientOriginalName();
            $location = public_path('uploads/slideImages/' . $image_name);
            Image::make($image)->save($location);
            $img_array[]=$image_name;
        }
      $slide->image =  implode("|", $img_array);
      $slide->save();
    }

    public function edit($id)
    {
        $list_page = $this->slide->PageList();
        $items = $this->slide->findSlide($id);
        return view($this->view.'.'.__FUNCTION__, compact('items','list_page'));
    }
    public function destroy($id)
    {
        $slide = $this->slide->findSlide($id);
        foreach(explode('|', $slide->image) as $filename)
        {
          $path = 'uploads/slideImages/';
          File::delete($path.$filename);
        }
        $slide->delete();
        Session::flash('success', 'Destroy user was successfully save!');
        return back();
    }

}
