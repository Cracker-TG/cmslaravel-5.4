<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Posts;
use App\Models\Post;
use App\Http\Requests\PostRequest;
use Auth;
use Session;
use Image;
use File;

class PostsController extends MainController
{

    private $view = 'backend.posts';


    public function __construct(Posts $posts)
    {
        $this->posts = $posts;
    }


    public function index()
    {

        $items = $this->posts->all();
        return view($this->view.'.'.__FUNCTION__,compact('items'));

    }

    public function create()
    {
        $categories = $this->posts->getCategories();

        return view($this->view.'.'.__FUNCTION__,compact('categories'));

    }
    public function store(PostRequest $request)
    {

      $post = Post::create($request->except(['user_id','category_id','cover']));
      $post->user_id = Auth::user()->id;
      if($request->has('category_id'))
      {

        foreach ($request->category_id as $category_id)
        {
				      $post->categories()->attach($category_id);
			  }

      }
      if($request->hasFile('cover'))
      {
        $image = $request->file('cover');
        $image_name = $image->getClientOriginalName();
        $location = public_path('uploads/postImages/' . $image_name);
        Image::make($image)->save($location);
        $post->cover = $image_name;
      }


      $post->save();
      Session::flash('success', 'Create pages was successfully save!');
      return redirect()->route('posts.index');
    }
    public function edit($id)
    {
      $post = $this->posts->findPost($id);
      $edit_categories = $this->posts->getCategoriesFormRelation($id);
      $categories = $this->posts->getCategories();
      return view($this->view.'.'.__FUNCTION__, compact('post','categories','edit_categories'));
    }

    public function update(PostRequest $request, $id)
    {
        $post = $this->posts->findPost($id);
        if($request->has('category_id'))
        {
          $post->categories()->detach();
          foreach ($request->category_id as $category_id)
          {
            $post->categories()->attach($category_id);
          }
        }
        if($request->hasFile('cover'))
        {
          $path = 'uploads/postImages/';
          File::delete($path.$post->cover);

          $image = $request->file('cover');
          $image_name = $image->getClientOriginalName();
          $location = public_path($path.$image_name);
          Image::make($image)->save($location);
          $post->cover = $image_name;
        }
        $post->title = $request->title;
        $post->is_visible = $request->is_visible;
        $post->content = $request->content;
        $post->tags = $request->tags;
        $post->summary = $request->summary;
        $post->slug = $request->slug;
        $post->save();
        Session::flash('success', 'Update user was successfully save!');
        return redirect()->route('posts.index');
    }

    public function destroy($id)
    {
        $path = 'uploads/postImages/';
        $post = $this->posts->findPost($id);
        File::delete($path.$post->cover);
        $post->categories()->detach();
        $post->delete();
        Session::flash('success', 'Destroy post was successfully save!');
        return back();
    }




  }
