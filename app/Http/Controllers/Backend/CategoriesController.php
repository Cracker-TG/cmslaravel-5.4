<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Categories;
use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use Auth;
use Session;

class CategoriesController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     private $view = 'backend.categories';
     public function __construct(Categories $categories)
     {
         $this->categories = $categories;
    }
    public function index()
    {
      $items = $this->categories->all();
      return view($this->view.'.'.__FUNCTION__,compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = $this->categories->all();
        return view($this->view.'.'.__FUNCTION__,compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
      $category = Category::create($request->except('user_id'));
      $category->user_id = Auth::user()->id;
      $category->save();
      Session::flash('success', 'Create category was successfully save!');
      return redirect()->route('categories.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $category = $this->categories->findCategory($id);
      return view($this->view.'.'.__FUNCTION__, compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
      $category = $this->categories->findCategory($id);
      $category->name = $request->name;
      $category->user_id = Auth::user()->id;
      $category->save();
      Session::flash('success', 'Update category was successfully save!');
      return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->categories->findCategory($id);
        $category->delete();
        Session::flash('success', 'Destroy user was successfully save!');
        return back();
    }
}
