<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;
use App\Models\Role;
use App\Repositories\Users;
use App\Http\Requests\UserRequest;

use Hash;
use Session;
use Image;
use File;

class UserManagementController extends MainController
{
    private $view = 'backend.users';

    protected $users;

    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    public function index()
    {
        $items = $this->users->all();
        return view($this->view.'.'.__FUNCTION__, compact('items'));
    }

    public function create()
    {
        $roles = $this->users->listRoles();
        return view($this->view.'.'.__FUNCTION__, compact('roles'));
    }

    public function store(UserRequest $request)
    {
        $user = User::create($request->except(['password']));
        $user->password = Hash::make($request->input('password'));

        $user->save();
        Session::flash('success', 'Create user was successfully save!');
        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $roles = $this->users->listRoles();
        $user = $this->users->findUser($id);
        return view($this->view.'.'.__FUNCTION__, compact('user', 'roles'));
    }
    public function update(UserRequest $request, $id)
    {
        $user = $this->users->findUser($id);
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        if($request->password != null)
        {
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();
        Session::flash('success', 'Update user was successfully save!');
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $user = $this->users->findUser($id);
        $user->delete();
        Session::flash('success', 'Destroy user was successfully save!');
        return back();
    }

    /*

    public function postUpload(Request $request)
    {

        $image = $request->file('file');
        $image_name = $image->getClientOriginalName();
        $location = public_path('uploads/UserAvatar/' . $image_name);
        Image::make($image)->save($location);
    }
    public function deleteUpload(Request $request)
    {
        $path = 'uploads/UserAvatar/';
        $file_name = $request->get('id');
        File::delete($path.$file_name);

    }
    */
    /*
    if ($request->hasFile('avatar')) {
        $image = $request->file('avatar');
        $image_name =$image->getClientOriginalName();
        $location = public_path('uploads/UserAvatar/' . $image_name);
        Image::make($image)->save($location);
        $user->avatar = $image_name;
    }
    */
}
