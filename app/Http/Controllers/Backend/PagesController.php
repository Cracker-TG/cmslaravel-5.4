<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Pages;
use App\Models\Page;
use App\Http\Requests\PageRequest;
use Hash;
use Auth;
use Session;
use Image;
use File;

class PagesController extends MainController
{

    private $view = 'backend.pages';


    public function __construct(Pages $pages)
    {
        $this->pages = $pages;
    }


    public function index()
    {

        $items = $this->pages->all();
        return view($this->view.'.'.__FUNCTION__,compact('items'));

    }

    public function create()
    {

        return view($this->view.'.'.__FUNCTION__);

    }

    public function store(PageRequest $request)
    {
      $page = Page::create($request->except('user_id'));
      $page->user_id = Auth::user()->id;
      $page->save();
      Session::flash('success', 'Create pages was successfully save!');
      return redirect()->route('pages.index');
    }

    public function edit($id)
    {

      $page = $this->pages->findPage($id);

      return view($this->view.'.'.__FUNCTION__, compact('page'));
    }

    public function update(PageRequest $request, $id)
    {
        $page = $this->pages->findPage($id);
        $page->title = $request->title;
        $page->content = $request->content;
        $page->tags = $request->tags;
        $page->style = $request->style;
        $page->script = $request->script;
        $page->slug = $request->slug;
        $page->save();
        Session::flash('success', 'Update user was successfully save!');
        return redirect()->route('pages.index');
    }

    public function destroy($id)
    {
        $page = $this->pages->findPage($id);
        $page->delete();
        Session::flash('success', 'Destroy user was successfully save!');
        return back();
    }


  }
