<?php

namespace App\Http\Controllers;
use \App\Models\Post;
use Illuminate\Http\Request;
use App\Repositories\Posts;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     private $view = 'posts';

    public function __construct(Posts $posts)
    {
          $this->posts = $posts;

    }


      public function index()
      {
          $posts = $this->posts->showLastSix();
          return view($this->view.'.'.__FUNCTION__,compact('posts'));
      }
      public function show(Post $post)
      {
          return view($this->view.'.'.__FUNCTION__,compact('post'));
      }



}
