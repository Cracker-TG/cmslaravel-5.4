<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/home', 'PageController@getHome')->name('home');
Route::get('/', 'PageController@getIndex')->name('index');
Route::resource('/pages', 'PageController');
  Route::get('/posts','PostsController@index')->name('posts');
  Route::get('/posts/{post}/{slug}/','PostsController@show')->name('posts.show');
/**
 * These are admin routes
 *
 *
 */
Route::prefix('admin')->middleware('admin')-> group(function()
{
  Route::get('/', 'Backend\DashboardController@index')->name('admin.dashboard');
  Route::resource('users', 'Backend\UserManagementController');
  Route::post('/users/upload', 'Backend\UserManagementController@postUpload')->name('avatar-upload');
  Route::post('/users/upload/delete', 'Backend\UserManagementController@deleteUpload')->name('delete-upload');
  Route::resource('menus', 'Backend\MenusController');
  Route::resource('pages', 'Backend\PagesController');
  Route::post('menus/order', 'Backend\MenusController@order')->name('order.menu');
  Route::resource('slides', 'Backend\SlidesController');
  Route::resource('categories', 'Backend\CategoriesController');
  Route::resource('posts', 'Backend\PostsController');
  Route::get('setting/headerfooter','Backend\SettingController@headerFooterEdit')->name('HFE');
  Route::PATCH('setting/headerfooter','Backend\SettingController@headerFooterUpdate')->name('HFU');
  Route::get('setting/logo','Backend\SettingController@logoEdit')->name('logo-edit');
  Route::PATCH('setting/logo','Backend\SettingController@logoUpdate')->name('logo-update');
});
