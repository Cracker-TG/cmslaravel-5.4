<?php

use Illuminate\Database\Seeder;

class HeaderFooterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('headers-footers')->insert
      ([
        'header' => null,
        'footer' => null,
      ]);
    }
}
